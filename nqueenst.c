#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <math.h>


typedef struct argumentosThread	{	// Estrutura que armazena os dados passados para as threads
	int dim, queens, posicoes;
	long long total;
	int *posicCertas;				// Vetor que guarda as posicoes das rainhas de uma matriz que deu certo
	int head;						// tamanho do vetor

}ArgumentosThread;

pthread_mutex_t mutexValor = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexVetor = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexPosic = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexPrinta = PTHREAD_MUTEX_INITIALIZER;		//Este mutex e so para nao printar uma matriz dentro de outra


void iniciaMatriz (void *argumentos);
ArgumentosThread * inicializaArgumentos(int dim, int queens, int posicoes);
int calculaAtaque (int posic1, int posic2, int dim, int matriz[dim][dim]);
int setaRainha (int dim, int matriz[dim][dim], int posicao, void *argumentos);
int printaMatriz (int dim, int matriz[dim][dim]);
int calculaValor (int dim, int matriz[dim][dim], void *argumentos);
int matrizJaCalculada (int dim, int matriz[dim][dim], void *argumentos);
int recursao (int dim, int matriz[dim][dim], void *argumentos, int queens);



unsigned long long nqueens(int dim, int queens) {
	int posicoes;
	int i=0;
	//int cont=0;
	posicoes = dim*dim;
	
	pthread_t thread[posicoes-1];
	
	if (dim <= 0 || queens <= 0){
		return 0;
	}
	
	
	ArgumentosThread *argumentosThread;
	argumentosThread = inicializaArgumentos(dim, queens, posicoes);		// Inicializa os argumentos
	
	for (i=0; i<9999999; i++){					// inicializa o vetor todo com o valor -1
		argumentosThread->posicCertas[i] = -1;
	}
	
	for(i = 0; i < posicoes; i++) {
		pthread_create(&thread[i], NULL, (void *)iniciaMatriz, (void *) argumentosThread);	// Cria as threads
	}
	
	for(i = 0; i < posicoes; i++) {
		pthread_join(thread[i], NULL);		// Espera as threads acabarem
	} 
	//printf("\n%d\n", argumentosThread->total);
	/*for (i=0, cont=0; i<=argumentosThread->head-1; i++, cont++){
		printf("%d ", argumentosThread->posicCertas[i]);
		if(cont == argumentosThread->queens-1){
			cont = -1;
			printf(",");	
		}
	}*/
	return argumentosThread->total;
	
}

ArgumentosThread * inicializaArgumentos(int dim, int queens, int posicoes) {
	ArgumentosThread *argumentos;

	argumentos = malloc(sizeof(ArgumentosThread));		// Aloca a struct
	argumentos->dim = dim;
	argumentos->queens = queens;
	argumentos->posicoes = posicoes;
	argumentos->total = 0;
	argumentos->posicCertas = malloc(sizeof(int) * 9999999);
	argumentos->head = 0;
	return argumentos;
}

void iniciaMatriz (void *argumentos) {
	ArgumentosThread *args;
	args = (ArgumentosThread *) argumentos;
	
	int matriz [args->dim-1][args->dim-1], posicao=0;	
	int i = 0;
	int j = 0;
	for (i=0; i<args->dim; i++){		// inicia a matriz com 0 em todas as posicoes ( 0 = posicao livre ,  1 = posicao com rainha,  -1 = posicao de ataque de uma rainha)
		for (j=0; j<args->dim; j++) {
			matriz[i][j] = 0;
		}
	}
	pthread_mutex_lock(&mutexPosic);		//adquiri o mutex
	posicao = args->posicoes;
	args->posicoes--;
	pthread_mutex_unlock(&mutexPosic);		//libera o mutex
	
	setaRainha(args->dim,matriz,posicao,(void *) args);
}

int setaRainha (int dim, int matriz[dim][dim], int posicao, void *argumentos) { // funcao que coloca a primeira rainha
	ArgumentosThread *args;
	args = (ArgumentosThread *) argumentos;
	int i=0,j=0,contaposic=0, posic1=0, posic2=0;
	for (i=0; i<dim; i++){
		for (j=0; j<dim; j++){
			if (contaposic == posicao-1){
				posic1 = i;
				posic2 = j;
				matriz[i][j] = 1;
				contaposic++;
				calculaAtaque(posic1,posic2,dim,matriz);
				recursao(dim,matriz,(void *) args, args->queens-1);				
					
			} else{
				contaposic++;
			}
		}
	}
	return 0;
}

int recursao (int dim, int matriz[dim][dim], void *argumentos, int queens){
	ArgumentosThread *args;
	args = (ArgumentosThread *) argumentos;
	if (queens <= 0) {
		if (!(matrizJaCalculada(dim, matriz, (void *) args))){
			calculaValor(dim, matriz, (void *) args);
		}
		
	}  else {
		int matriz_aux[dim][dim],i=0, j=0, x=0, y=0, posic1=0, posic2=0;
		for (i=0; i<dim; i++){
			for (j=0; j<dim; j++){
				if (matriz[i][j] == 0){
					matriz[i][j] = 2;
					posic1 = i;
					posic2 = j;
					for (x=0; x<dim; x++){		// laco que faz a copia da matriz original para uma auxiliar
						for (y=0; y<dim; y++){
							matriz_aux[x][y] = matriz[x][y];
						}
					}
					recursao(dim,matriz_aux,(void *) args, queens);
					matriz[i][j] = 1;
					calculaAtaque(posic1,posic2,dim,matriz);
					for (x=0; x<dim; x++){		// laco que faz com que as posicoes ja verificadas voltem a ser vazias
						for (y=0; y<dim; y++){
							if (matriz[x][y] == 2){
								matriz[x][y] = 0;
							}
						}
					}
					for (x=0; x<dim; x++){		// laco que faz a copia da matriz original para uma auxiliar
						for (y=0; y<dim; y++){
							matriz_aux[x][y] = matriz[x][y];
						}
					}
					recursao(dim,matriz_aux,(void *) args, queens-1);
				}
			}
		}
	}
	return 0;
}

int matrizJaCalculada (int dim, int matriz[dim][dim], void *argumentos){
	ArgumentosThread *args;
	args = (ArgumentosThread *) argumentos;

	int i=0, j=0, contaposic=0, aux=0, retorno = 0, posicRainhas[args->queens];
	for (i=0; i<dim; i++){		// laco que coloca as posicoes das rainhas em um vetor
		for (j=0; j<dim; j++){
			if (matriz[i][j]== 1) {
				posicRainhas[aux]=contaposic;
				contaposic++;
				aux++;
				if (aux > args->queens){
					return 1;
				}
			} else {
				contaposic++;
			}			
		}
	}
	aux = 0;
	pthread_mutex_lock(&mutexVetor);		//adquiri o mutex
	for (i=0; i<=args->head; i++){
		if (posicRainhas[aux] == args->posicCertas[i]){		// testa se a posicao da matriz ja foi calculada
			aux++;
			if (aux == args->queens){		// matriz igual
				retorno = 1;
				pthread_mutex_unlock(&mutexVetor);		// libera o mutex
				return retorno;
			}	
		} else {
					i = i+ args->queens - aux - 1;
					aux = 0;
		}
	}
	retorno = 0;
	return retorno;
}

int calculaValor (int dim, int matriz[dim][dim], void *argumentos) {
	ArgumentosThread *args;
	args = (ArgumentosThread *) argumentos;
	int i=0,j=0, contaposic=0;
	long long aux=0;
	for (i=0; i<dim; i++){
		for (j=0; j<dim; j++){
			if (matriz[i][j] == 1){
				aux = aux+(pow(2,contaposic));
				args->posicCertas[args->head]= contaposic;
				args->head++;
				contaposic++;
			} else {
				contaposic++;
			}
		}
	}
	pthread_mutex_lock(&mutexValor);		//adquiri o mutex
	args->total = args->total + aux;
	pthread_mutex_unlock(&mutexValor);		// libera o mutex
	pthread_mutex_unlock(&mutexVetor);		// libera o mutex
	return 0;	
	
}

int calculaAtaque (int posic1, int posic2, int dim, int matriz[dim][dim]){ // funcao que coloca as posicoes de um ataque de uma rainha na matriz
	int i=posic1-1;
	int j=posic2-1;
	while (i<dim && j<dim && i>=0 && j>=0){ // Calcula o ataque da rainha da parte superior esquerda
		matriz[i][j] = -1;
		i--;
		j--;
	}
	i=posic1-1;
	j=posic2;
	while (i<dim && i>=0){ // Calcula o ataque da rainha da parte superior
		matriz[i][j] = -1;
		i--;
	}
	i=posic1-1;
	j=posic2+1;
	while (i<dim && j<dim && i>=0 && j>=0){ // Calcula o ataque da rainha da parte superior direita
		matriz[i][j] = -1;
		i--;
		j++;
	}
	i=posic1;
	j=posic2-1;
	while (i<dim && j<dim && i>=0 && j>=0){ // Calcula o ataque da rainha da parte esquerda
		matriz[i][j] = -1;
		j--;
	}
	
	i=posic1;
	j=posic2+1;
	while (i<dim && j<dim && i>=0 && j>=0){ // Calcula o ataque da rainha da parte direta
		matriz[i][j] = -1;
		j++;
	}
	i=posic1+1;
	j=posic2-1;
	while (i<dim && j<dim && i>=0 && j>=0){ // Calcula o ataque da rainha da parte inferior esquerda
		matriz[i][j] = -1;
		i++;
		j--;
	}
	i=posic1+1;
	j=posic2;
	while (i<dim && i>=0){ // Calcula o ataque da rainha da parte inferior
		matriz[i][j] = -1;
		i++;
	}
	i=posic1+1;
	j=posic2+1;
	while (i<dim && j<dim && i>=0 && j>=0){ // Calcula o ataque da rainha da parte inferior direita
		matriz[i][j] = -1;
		i++;
		j++;
	}
	return 0;
}

int printaMatriz (int dim, int matriz[dim][dim]){ // funcao que printa a matriz
	pthread_mutex_lock(&mutexPrinta);		//adquiri o mutex
	int i=0,j=0;
	for (i=0; i<dim; i++){
		for (j=0; j<dim; j++){
			if (i==0 && j==0){
				printf("{%d,", matriz[i][j]);			
			} else if (i==dim-1 && j==dim-1){
				printf("%d}\n", matriz[i][j]);
			} else if (j==dim-1) {
				printf("%d,\n", matriz[i][j]);
				} else  {
				printf(" %d,", matriz[i][j]);
			}
		}
	}
	printf("=====================\n");
	pthread_mutex_unlock(&mutexPrinta);		//adquiri o mutex
	return 0;
}
